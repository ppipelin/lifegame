#include "gamemanager.h"


GameManager::GameManager(Game *game):
    m_timer(new QTimer(this)),
    m_gen(0),
    m_interval(20),
    m_running(false),
    m_game(game),
    m_tracing(false),
    m_erasing(false),
    m_tracing_while_running(false),
    m_erasing_while_running(false),
    m_canTrace(true)
{
    m_timer->setInterval(m_interval);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(newGeneration()));


    m_lastCellPointed.setX(0);
    m_lastCellPointed.setY(0);
}

GameManager::~GameManager()
{
    delete m_timer;
}

void GameManager::startStop()
{
    m_running ? stopGame() : startGame();
}

void GameManager::startGame()
{
    if(!m_running){
        qDebug() << "Resuming game";
        m_timer->start();
        emit startStopSignal(true);
        m_running = true;
    }
}

bool GameManager::running()
{
    return m_running;
}

void GameManager::stopGame()
{
    if(m_running){
        qDebug() << "Stopping game";
        m_timer->stop();
        emit startStopSignal(false);
        m_running = false;
    }
}

void GameManager::clear()
{
    m_game->clear();
    m_gen = 0;
    emit updatePop(0);
    update();
}

void GameManager::cellPointed(qreal y, qreal x, int &i, int &j)
{
    i = int(floor(y));
    j = int(floor(x));
}

void GameManager::traceLine(int x1, int y1, int x2, int y2, bool isErasing)
{
    quint8 val = !isErasing;
    m_game->set(x2,y2,val); emit updatePop(m_game->pop());
    // Bresenham's line algorithm
    const bool steep = (abs(y2 - y1) > abs(x2 - x1));
    if(steep){
        std::swap(x1, y1);
        std::swap(x2, y2);
    }
    if(x1 > x2){
        std::swap(x1, x2);
        std::swap(y1, y2);
    }
    const int dx = x2 - x1;
    const int dy = abs(y2 - y1);
    int error = dx / 2;
    const int ystep = (y1 < y2) ? 1 : -1;
    int y = y1;
    const int maxX = x2;

    for(int x= x1; x<maxX; x++)
    {
        steep ? m_game->set(y,x,val) : m_game->set(x,y,val);  emit updatePop(m_game->pop());
        error -= dy;
        if(error < 0){
            y += ystep;
            error += dx;
        }
    }

}

void GameManager::reset()
{
    qDebug() << "Game cleared";
    clear();
    stopGame();
    emit updateGen(0);
}

void GameManager::tracePermission(bool b)
{
    m_canTrace = b;
}

void GameManager::setGen(unsigned int)
{
    m_gen = 0;
}

void GameManager::newGeneration()
{

    double time = (m_gen * m_interval)*0.001;
    if (fmod(time,1.) == 0) qDebug() << time<<"s"<<m_gen<<"gen";
    //calcul de la gen suivante
    if(!m_game->step()){
        QMessageBox inf(QMessageBox::Icon::Information,
        tr("Le jeu s'est arrêté."),
        tr("Toutes les générations seront identiques."),
        QMessageBox::Ok);
        inf.setWindowIcon(QIcon(":/img/images/iconGameOfLife.gif"));
        inf.exec();
        stopGame();

    }else{
        m_gen++;
        emit updateGen(m_gen);
        emit updatePop(m_game->pop());
    }


}

void GameManager::onClick(const QPointF & p, bool b)
{
    if(m_canTrace){
        //Si c'est un clic gauche et qu'on est pas en train d'effacer
        if(b && !m_erasing){
            m_tracing = true;
            if(m_running){//Si le jeu est est en cours, on le pause et on est dans l'état "je trace pendant le jeu"
                m_tracing_while_running = true;
                stopGame();
            }
            int i,j;
            cellPointed(p.y(), p.x(), j,i);
            quint8 val = m_game->get(i,j);
            if (val == 0){
                m_game->set(i,j, 1);
                emit updatePop(m_game->pop());
            }
        }
        //Si c'est un clic droit et qu'on est pas en train de tracer
        else if(!b && !m_tracing){
            m_erasing = true;
            if(m_running){ //Si le jeu est est en cours, on le pause et on est dans l'état "j'éfface pendant le jeu"
                m_erasing_while_running = true;
                stopGame();
            }
            int i,j;
            cellPointed(p.y(), p.x(), j,i);
            quint8 val = m_game->get(i,j);
            if(val==1){
                m_game->kill(i,j);
                emit updatePop(m_game->pop());
            }
        }
    }
}

void GameManager::onRelease(bool b)
{
    //release gauche et on n'est pas en train d'effacer, alors on est plus en train de tracer
    if(b && !m_erasing){
        m_tracing = false;
        if(m_tracing_while_running){//Si le jeu tournait, on n'est plus dans l'état "pause pendant que je trace", et on le relance
            m_tracing_while_running = false;
            startGame();
        }
    }
    //release droit et on n'est pas en train de tracer, alors on est plus en train d'effacer
    else if(!b && !m_tracing){
         m_erasing = false;
         if(m_erasing_while_running){//Si le jeu tournait, on n'est plus dans l'état "pause pendant que j'efface", et on le relance
             m_erasing_while_running = false;
             startGame();
         }
    }
}

void GameManager::onMove(const QPointF & p)
{
    if(m_canTrace){
        int i,j;
        cellPointed(p.y(), p.x(), j, i);

        if(m_tracing){
            traceLine(m_lastCellPointed.x(), m_lastCellPointed.y(), i, j, false);
        }else if(m_erasing){
            traceLine(m_lastCellPointed.x(), m_lastCellPointed.y(), i, j, true);
        }
        if(i !=m_lastCellPointed.x() ) m_lastCellPointed.setX(i);
        if(j !=m_lastCellPointed.y() ) m_lastCellPointed.setY(j);
    }

}

void GameManager::setInterval(int inter){
    m_interval = inter;
    qDebug()<<m_interval<<"ms";
    m_timer->setInterval(m_interval);
}

void GameManager::selectStructure(int)
{

}
