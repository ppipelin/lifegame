#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H
#include "game.h"
#include"gamescene.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QMessageBox>
#include <QPoint>
#include <QIcon>

/* Classe GameManager :
    C'est lui qui gère le jeu (appel de step(), clic de souris...)

 Attributs :
    un QTimer*
    un Game*
*/

class GameManager :public QWidget
{
    Q_OBJECT
private:

    QTimer * m_timer;
    unsigned int m_gen, m_interval;
    bool m_running;
    Game * m_game;
    bool m_tracing, m_erasing;
    bool m_tracing_while_running;
    bool m_erasing_while_running;
    QPoint m_lastCellPointed;
    bool m_canTrace;
    void clear();// reset the game
    void cellPointed(qreal y, qreal x, int &i, int &j);

    void traceLine(int, int, int, int, bool);


public slots:
    void startStop();
    void reset();
    void tracePermission(bool);
    void setGen(unsigned int);

signals:
    void startStopSignal(bool);
    void updateGen(unsigned int);
    void updatePop(const unsigned int &);

public:
    GameManager(Game * game);
    ~GameManager();
    void stopGame();
    void startGame();
    bool running();

    void setInterval(int inter);
private slots:
    void onClick(const QPointF &, bool);
    void onRelease(bool);
    void onMove(const QPointF &);
    void selectStructure(int); //int est l'index dans le tableau des stuctures possibles chargé au lancement
public slots:
    void newGeneration();


};

#endif // GAMEMANAGER_H
