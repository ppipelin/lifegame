#-------------------------------------------------
#
# Project created by QtCreator 2019-01-14T17:16:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LifeGame
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    rule.cpp \
    graphics_view_zoom.cpp \
    gamemanager.cpp \
    colors.cpp \
    settingswindow.cpp \
    helpwindow.cpp

HEADERS += \
        mainwindow.h \
    parser.h \
    sparse.h \
    game.h \
    rule.h \
    golrule.h \
    gamescene.h \
    gameview.h \
    graphics_view_zoom.h \
    gamemanager.h \
    colors.h \
    cell.h \
    settingswindow.h \
    helpwindow.h

FORMS += \
        mainwindow.ui \
    settingswindow.ui \
    helpwindow.ui

RESOURCES += \
    lib.qrc

QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp
