#pragma once
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include "gameview.h"
#include "graphics_view_zoom.h"
#include "gamemanager.h"
#include "golrule.h"
#include "sparse.h"
#include <QFile>
#include <QFileDialog>
#include "parser.h"
#include <QStatusBar>
#include "settingswindow.h"
#include "helpwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget * parent = nullptr);
    ~MainWindow();

public slots:
    void saveGame();
    void saveGameUnder();
    void importGame(int);
    void loadSparse();
    void loadRule();
    void openGame();
    void updateIcon(bool);
    void onChanges();
    void newFile();
    void backToLastFile();
    void setGen(unsigned int);
    void setPop(const unsigned int &);
    void setSpeedFromSlider(int);
    void setSpeedFromSpinBox(int);
    void openSettings();
    void loadGame();
    void help();

private:
    Ui::MainWindow * m_ui;
    //GameWidget * m_gameWindow;
    QIcon m_resetIcon, m_startIcon, m_stopIcon, m_clearIcon, m_stepIcon, m_recenterIcon, m_helpIcon;
    GameScene m_scene;
    GameView m_view;
    Sparse * m_sparse;
    Graphics_view_zoom * m_zoom;
    GameManager * m_gm;
    Rule * m_rule;
    Game * m_game;
    QString fileName ="";
    bool changes;

    //void keyPressEvent(QKeyEvent *event);
    void saveInFile(const QString &);
    void updateTitle();



};

#endif // MAINWINDOW_H
