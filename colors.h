#ifndef COLORS_H
#define COLORS_H
#include <vector>
#include <QColor>
#include <random>

class Colors
{
private:
    static std::vector<QColor> colors;

    // Pourquoi ne pas initialiser les couleurs au hasard ?
    static std::vector<QColor> init(){
        std::vector<QColor> cols(256,Qt::black);
        for(int i = 1 ; i < 256 ; ++i){
            QColor random = QColor(std::rand()%256, std::rand()%256, std::rand()%256);
            cols[i] = random;
        }
        return cols;
    }
public:
    void update(const std::vector<QColor> & cols){
         colors = cols;
    }
    static QColor get(quint8 val){
        if(val == 200) return  QColor(200,200,200);
        return val-1 < int(colors.size()) ? colors[size_t(val-1)] : QColor(100,100,0); // <- couleur par defaut si bug !

    }


};

#endif // COLORS_H
