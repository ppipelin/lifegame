#ifndef GRAPHICS_VIEW_ZOOM_H
#define GRAPHICS_VIEW_ZOOM_H
#include <QObject>
#include <QGraphicsView>
#include <QDebug>
#include <qmath.h>
/* Classe trouvée sur internet :
    Permet de zoomer sur l'endroit ou se trouve la souris, ce qui n'est pas trivial.
    Pour la faire fonctionner sur une vue il suffit de l'initialiser -> exemple : Graphics_view_zoom zoom(ma_vue);

    Il y est rajouté le signal qui donne la permission à la scene d'afficher la grille fine.
    Aussi, le zoom est stopé suivant si on zoom trop ou si on dezoom trop.
 * /


/*!
* This class adds ability to zoom QGraphicsView using mouse wheel. The point under cursor
* remains motionless while it's possible.
*
* Note that it becomes not possible when the scene's
* size is not large enough comparing to the viewport size. QGraphicsView centers the picture
* when it's smaller than the view. And QGraphicsView's scrolls boundaries don't allow to
* put any picture point at any viewport position.
*
* When the user starts scrolling, this class remembers original scene position and
* keeps it until scrolling is completed. It's better than getting original scene position at
* each scrolling step because that approach leads to position errors due to before-mentioned
* positioning restrictions.
*
* When zommed using scroll, this class emits zoomed() signal.
*
* Usage:
*
* new Graphics_view_zoom(view);
*
* The object will be deleted automatically when the view is deleted.
*
* You can set keyboard modifiers used for zooming using set_modified(). Zooming will be
* performed only on exact match of modifiers combination. The default modifier is Ctrl.
*
* You can change zoom velocity by calling set_zoom_factor_base().
* Zoom coefficient is calculated as zoom_factor_base^angle_delta
* (see QWheelEvent::angleDelta).
* The default zoom factor base is 1.0015.
*/
class Graphics_view_zoom : public QObject {
    Q_OBJECT
public:
    Graphics_view_zoom(QGraphicsView* view);
    void gentle_zoom(double factor);
    void set_modifiers(Qt::KeyboardModifiers modifiers);
    void set_zoom_factor_base(double value);

private:
    double zoom;
    int cpt;
    QGraphicsView * _view;
    Qt::KeyboardModifiers _modifiers;
    double _zoom_factor_base;
    QPointF target_scene_pos, target_viewport_pos;
    bool eventFilter(QObject* object, QEvent* event);

public slots:
    void resetZoom(){
        //double factor = qPow(_zoom_factor_base,-cpt*120);
        double factor = qPow(_zoom_factor_base,-zoom);
        zoom=0;
        _view->scale(factor, factor);
        _view->centerOn(0,0);
        emit drawBackground(true);
        emit tracePermissionSignal(true);
    }

signals:
    void drawBackground(bool);
    void tracePermissionSignal(bool);
};

#endif // GRAPHICS_VIEW_ZOOM_H
