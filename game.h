#pragma once
#ifndef GAME_H
#define GAME_H
#include "sparse.h"
#include "rule.h"
#include <set>
#include <QElapsedTimer>
#include <QtConcurrent/QtConcurrent>

/* Classe Game :

 Attributs :
    *un Sparse, qui contient l'information sur l'état de la grille
    *une regle, qui est utilisée pour calculer la gen suivante
 Methodes clefs :
    *bool step() calcule la gen suivante et renvoie vrai si ca n'a rien fait
    * QSrting toLif() rend le .lif en qstring de l'etat actuel du jeu
*/

class Game : public QObject
{
    Q_OBJECT
signals:
    void hasChanged();
    void updateGen(unsigned int);
public slots:

public :
    typedef set<tuple<int,int,quint8>> TripletSet;
private:
    Rule * m_rule;
    Sparse * m_sparse;
    int cpt = 1;

public:
    Game(Rule * rule, Sparse * sparse):m_rule(rule),m_sparse(sparse)
    {
    }

    virtual ~Game()
    {}

    void setRule(Rule * rule){
        m_rule = rule;
        qDebug()<<"nouvelle regle:"<<m_rule->ruleLine();
        emit hasChanged();
        emit updateGen(0);
    }

    quint8 get(int i, int j) const{
        return m_sparse->get(i,j);
    }

    void set(int i, int j, quint8 val){
        m_sparse->set(i,j,val);
        emit hasChanged();
        emit updateGen(0);
    }

    void kill(int i, int j){
        set(i,j,0);
    }

    void merge(const Sparse & S){
        if(S.size()>0){
            emit hasChanged();
            emit updateGen(0);
        }
        m_sparse->merge(S);
    }
    void load(const Sparse & S){
        if(S.size()>0){
            emit hasChanged();
            emit updateGen(0);
        }
        m_sparse->load(S);
    }

    const unsigned int & pop() const{
        return m_sparse->pop();
    }

    void clear(){
        if(m_sparse->size() > 0) emit hasChanged();
        m_sparse->clear();
        cpt = 0;
    }

    int size(){
        return m_sparse->size();
    }

    bool step(){
        ++cpt;
        TripletSet deletion;
        TripletSet insertion;

        for(auto it = m_sparse->cbegin(); it != m_sparse->cend() ; ++it){
            int new_val = m_rule->new_state(it->val, it->nb);
            if(new_val != it->val){
                (new_val == 0 ? deletion : insertion).insert(make_tuple(it->i,it->j,new_val));
            }
        }

        // On préfère commencer par les insertions...
        for(auto it = insertion.begin(); it != insertion.end() ; ++it){
            m_sparse->set(std::get<0>(*it), std::get<1>(*it), std::get<2>(*it));
        }
        for(auto it = deletion.begin(); it != deletion.end() ; ++it){
            m_sparse->set(std::get<0>(*it), std::get<1>(*it), std::get<2>(*it));
        }

        bool changed = !insertion.empty() || !deletion.empty();
        if(changed){
            emit hasChanged();
        }
        return changed;
    }

   // Dans un premier temps, avec un etat binaire
   QString toLif(){
        QString headerLine = "#Life GK/PEP \n";
        QString ruleLine = m_rule->ruleLine();
        ruleLine.append("\n");
        QStringList pointBlocks;
        for(auto it = m_sparse->cbegin(); it != m_sparse->cend() ; ++it){
            if(it->val > 0){
                QString block =  "#P ";
                block.append(QString::number(it->i)).append(" ").append(QString::number(it->j));
                block.append("\n").append("*").append("\n");
                pointBlocks.append(block);
            }
        }
        QString res = headerLine + ruleLine;
        for(QString qs : pointBlocks){
            res += qs;
        }
        return res;
   }




};


#endif
