#ifndef GAMESCENE_H
#define GAMESCENE_H
#pragma once

#include <QDebug>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <QWidget>
#include <qmath.h>
#include <limits>

/* Classe GameScene :
    C'est la scene du jeu

    Il s'agit juste d'une QGraphicsScene avec une grille en fond
*/

class GameScene : public QGraphicsScene
{
    Q_OBJECT
private:
    unsigned int gridSize;
    bool drawingBackground;

public:
    explicit GameScene(QWidget * parent = nullptr)
        :QGraphicsScene (parent), gridSize(1), drawingBackground(true)
    {
        qreal size = std::numeric_limits<long long>::max()/2;
        setSceneRect(-size,-size,2*size, 2*size);
        qDebug()<<"création d'une grille de"<<2*size<<"par"<<2*size;
    }

    void drawBackground(QPainter * painter, const QRectF & rect){

        const int realLeft = static_cast<int>(std::floor(rect.left()));
        const int realRight = static_cast<int>(std::ceil(rect.right()));
        const int realTop = static_cast<int>(std::floor(rect.top()));
        const int realBottom = static_cast<int>(std::ceil(rect.bottom()));
        if(drawingBackground){


            // Draw grid.
            const int firstLeftGridLine = realLeft - (realLeft % gridSize);
            const int firstTopGridLine = realTop - (realTop % gridSize);

            QVarLengthArray<QLine, 100> lines;

            for (qreal x = firstLeftGridLine; x <= realRight; x += gridSize)
                lines.append(QLine(x, realTop, x, realBottom));
            for (qreal y = firstTopGridLine; y <= realBottom; y += gridSize)
                lines.append(QLine(realLeft, y, realRight, y));

            //painter->setRenderHint(QPainter::Antialiasing);
            painter->setPen(QPen(QColor(220, 220, 220), 0.0));
            painter->drawLines(lines.data(), lines.size());

            // Draw axes.

        }

        painter->setPen(QPen(Qt::lightGray, 0.0));
        painter->drawLine(0, realTop, 0, realBottom);
        painter->drawLine(realLeft, 0, realRight, 0);
    }

    ~GameScene()
    {
    }

    unsigned int getGridSize(){
        return gridSize;
    }

public slots:
    void updateDrawingBackground(bool b){
        drawingBackground = b;
    }

};


#endif
