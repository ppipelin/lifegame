#ifndef CELL_H
#define CELL_H
#include <QGraphicsItem>
#include <QBrush>
#include <QPainter>
#include "colors.h"
#include <QStyleOptionGraphicsItem>

class Cell : public QGraphicsItem
{
    //Q_OBJECT
private :
    QRect rect;
    QBrush brush;
public:
    explicit Cell(int i, int j, int val, QWidget *parent = nullptr)
    {
        rect = QRect(i,j,1,1);
        brush = QBrush(Colors::get(val),Qt::SolidPattern);
    }

    QRectF boundingRect() const
    {
        return rect;
    }
    void update(int val){
        QColor col(Colors::get(val));
        brush.setColor(col);
    }
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
    {
        //Q_UNUSED(painter)
        //Q_UNUSED(option)
        Q_UNUSED(widget)
        painter->setClipRect(option->exposedRect);
        painter->setPen(Qt::NoPen);
        painter->setBrush(brush);
        painter->drawRect(rect);
    }


};

#endif // CELL_H
