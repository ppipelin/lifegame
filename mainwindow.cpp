#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow),
    //m_gameWindow(new GameWidget(interval,rule,this)),
    m_resetIcon(":/img/images/reset.png"),
    m_startIcon(":/img/images/play.png"),
    m_stopIcon(":/img/images/stop.png"),
    m_clearIcon(":/img/images/clear.png"),
    m_stepIcon(":/img/images/step.png"),
    m_recenterIcon(":/img/images/recenter.png"),
    m_helpIcon(":/img/images/help.png"),
    m_scene(this),
    m_view(this),
    m_rule(new GOLRule()),//(new GOLRule({0,0,1,1,1,1,0,0,0},{0,0,0,0,1,1,1,1,1},3)),
    changes(false)
  //On peut s'amuser avec ca : {1,0,1,0,0,0,0,0,0},{0,0,0,1,0,0,0,0,0} + ligne doite = triangle de serpienski avec des points
{
    m_ui->setupUi(this);
    //Enlever le ? et
    QApplication::setAttribute(Qt::AA_DisableWindowContextHelpButton);
    setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);

    setWindowIcon(QIcon(":/img/images/iconGameOfLife.gif"));
    // Desactiver le resizing
    setFixedSize(size());

    m_ui->clearButton->setIcon(m_clearIcon);
    m_ui->clearButton->setToolTip("Effacer toutes les cellules [ctrl+C]");
    m_ui->resetButton->setIcon(m_resetIcon);
    m_ui->resetButton->setToolTip("Réouvrir le dernier fichier ouvert [ctrl+U]");
    m_ui->startButton->setIcon(m_startIcon);
    m_ui->startButton->setToolTip("Lancer le jeu [espace]");
    m_ui->stepButton->setIcon(m_stepIcon);
    m_ui->stepButton->setToolTip("Génération suivante [G]");
    m_ui->recenterButton->setIcon(m_recenterIcon);
    m_ui->recenterButton->setToolTip("Recentrer la vue et remettre le zoom par defaut [ctrl+W]");
    m_ui->helpButton->setIcon(m_helpIcon);
    m_ui->helpButton->setToolTip("Aide");

    updateTitle();

    //m_view.setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
    m_sparse = new Sparse(&m_scene);
    m_game = new Game(m_rule, m_sparse);
    m_gm = new GameManager(m_game);

    m_ui->gameView->setScene(&m_scene);
    m_zoom = new Graphics_view_zoom(m_ui->gameView);
    m_zoom->set_modifiers(Qt::NoModifier);
    connect(&m_view, SIGNAL(resetZoomSignal()), m_zoom, SLOT(resetZoom()));
    connect(m_zoom, SIGNAL(tracePermissionSignal(bool)), m_gm, SLOT(tracePermission(bool)));
    m_ui->gameView->zoom(10); // Pas modifier ca

    connect(m_ui->startButton, SIGNAL(clicked()), m_gm,SLOT(startStop()));
    connect(m_ui->clearButton, SIGNAL(clicked()), m_gm,SLOT(reset()));
    connect(m_ui->resetButton, SIGNAL(clicked()), this,SLOT(backToLastFile()));
    connect(m_ui->stepButton, SIGNAL(clicked()), m_gm,SLOT(newGeneration()));
    connect(m_ui->recenterButton, SIGNAL(clicked()), &m_view, SLOT(recenter()));
    connect(m_ui->actionNouveau, SIGNAL(triggered()), this ,SLOT(newFile()));
    connect(m_ui->actionEnregistrer, SIGNAL(triggered()), this ,SLOT(saveGame()));
    connect(m_ui->actionEnregistrer_sous, SIGNAL(triggered()), this ,SLOT(saveGameUnder()));
    connect(m_ui->actionOuvrir, SIGNAL(triggered()), this ,SLOT(openGame()));
    connect(m_ui->actionImporterGrille, SIGNAL(triggered()), this ,SLOT(loadSparse()));
    connect(m_ui->actionImporterRegle, SIGNAL(triggered()), this ,SLOT(loadRule()));
    connect(m_ui->actionImporterJeu, SIGNAL(triggered()), this ,SLOT(loadGame()));
    connect(m_ui->helpButton, SIGNAL(clicked()), this, SLOT(help()));


    connect(m_ui->actionRegles, SIGNAL(triggered()), this ,SLOT(openSettings()));

    connect(m_game, SIGNAL(hasChanged()), this ,SLOT(onChanges()));
    connect(m_gm, SIGNAL(startStopSignal(bool)), this, SLOT(updateIcon(bool)));
    connect(m_ui->gameView, SIGNAL(clicked(const QPointF &, bool)), m_gm, SLOT(onClick(const QPointF &, bool)));
    connect(m_ui->gameView, SIGNAL(released(bool)), m_gm, SLOT(onRelease(bool)));
    connect(m_ui->gameView, SIGNAL(mouseMoved(const QPointF &)), m_gm, SLOT(onMove(const QPointF &)));
    connect(m_zoom, SIGNAL(drawBackground(bool)), &m_scene, SLOT(updateDrawingBackground(bool)));

    m_ui->nbGen->setText("0");
    connect(m_gm, SIGNAL(updateGen(unsigned int)), this, SLOT(setGen(unsigned int)));
    connect(m_game, SIGNAL(updateGen(unsigned int)), m_gm, SLOT(setGen(unsigned int)));
    connect(m_game, SIGNAL(updateGen(unsigned int)), this, SLOT(setGen(unsigned int)));
    m_ui->nbPop->setText("0");
    connect(m_gm, SIGNAL(updatePop(const unsigned int &)), this, SLOT(setPop(const unsigned int &)));

    connect(m_ui->speedSlider, SIGNAL(valueChanged(int)),this, SLOT(setSpeedFromSlider(int)));
    connect(m_ui->speedSpinBox, SIGNAL(valueChanged(int)),this, SLOT(setSpeedFromSpinBox(int)));
}

MainWindow::~MainWindow()
{
    delete m_ui;
    delete m_zoom;
    delete m_sparse;
    delete m_rule;
    delete m_game;
    delete m_gm;
}

void MainWindow::updateTitle(){ //and resetButton...
    QString s = "Jeu de la vie - ";
    QString sfile = fileName.isEmpty() ? "Sans titre" : fileName;
    s.append(sfile);
    if(changes){
        s.append("*");
    }
    setWindowTitle(s);
    m_ui->resetButton->setEnabled(!fileName.isEmpty() && changes);
}

void MainWindow::onChanges(){
    changes = true;
    updateTitle();
}

void MainWindow::saveGameUnder(){
    setEnabled(false);
    if(m_gm->running()){
        m_gm->startStop();
    }


    QString sfile = QFileDialog::getSaveFileName(this,
            tr("Enregistrer l'état du jeu sous..."), "",
            tr("LIF (*.lif *.life);;Tous les fichiers (*)"));
    if (sfile.isEmpty()){
        setEnabled(true);
        return;
    }
    else{
        if(!fileName.isEmpty() && changes){

            QMessageBox question(QMessageBox::Question,
                                           "Enregistrement des modifications",
                                           "Voulez-vous enregistrer les modifications apportées à " + fileName+" ?",
                                            QMessageBox::Yes|QMessageBox::No
                                           );
            question.setButtonText(QMessageBox::Yes, tr("Oui, enregistrer les modifications"));
            question.setButtonText(QMessageBox::No, tr("Non, ignorer les modifications"));
            question.setIcon(QMessageBox::Warning);
            question.setWindowIcon(QIcon(":/img/images/iconGameOfLife.gif"));
            if(question.exec()== QMessageBox::Yes){
                qDebug()<<"enregistré";
                saveInFile(fileName);
            }else{
                qDebug()<<"pas enregistré";
            }
        }
        saveInFile(sfile);
        fileName = sfile;
        changes = false;
        updateTitle();
        qDebug() << "Le jeu a été enregistré dans"<<sfile;
        setEnabled(true);
    }

}

void MainWindow::saveGame()
{
    if(changes || fileName.isEmpty()){
        if(fileName == ""){
            saveGameUnder();
        }else{
            saveInFile(fileName);
            changes = false;
            updateTitle();
        }
    }
}

void MainWindow::saveInFile(const QString & sfile){
    QFile file(sfile);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("L'opération n'a pas pu être effectuée. Il se peut que le fichier indiqué soit protégé en écriture"),
            file.errorString());
        return;
    }
    QTextStream out(&file);
    QString lif = m_game->toLif();
    out << lif;
    //qDebug()<<lif;
}

void MainWindow::openGame()
{
    importGame(0);
    //0 -> met le fichier cible en fichier courant et importe la regle et la grille
}

void MainWindow::loadSparse()
{
    importGame(1);
    //1 -> garde le fichier courant et importe seulement la grille
}

void MainWindow::loadRule()
{
    importGame(2);
    //2 -> garde le fichier courant et importe seulement la grille
}

void MainWindow::loadGame()
{
    importGame(3);
    //2 -> garde le fichier courant et importe seulement la grille
}

void MainWindow::help()
{
    HelpWindow help;
    setEnabled(false);
    if(m_gm->running()){
        m_gm->startStop();
    }
    help.setModal(true);
    help.exec();
    setEnabled(true);
}

void MainWindow::importGame(int b){
    setEnabled(false);
    if(m_gm->running()){
        m_gm->startStop();
    }
    QString sfile = QFileDialog::getOpenFileName(this,
            tr(b==0 ? "Ouvrir un fichier lif" : "Charger une grille à partir d'un fichier lif..."),
            "",
            tr("LIF (*.lif *.life);;Tous les fichiers (*)"));
    if (sfile.isEmpty()){
        setEnabled(true);
        return;
    }
    else
    {
        QFile file(sfile);
        if (!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(this, tr("L'opération n'a pas pu être effectuée. Il se peut que le fichier indiqué soit protégé en lecture"),
                file.errorString());
            setEnabled(true);
            return;
        }
        QTextStream in(&file);
        //qDebug()<<in.readAll();
        Parser p(in);
        if(b==0 || b == 1|| b==3){
            Sparse * S = p.sparse();
            m_game->load(*S);
            this->setPop(m_game->pop());
            delete S;
        }
        if(b==0 || b==2 || b==3){
            delete m_rule;
            m_rule = p.rule();
            m_game->setRule(m_rule);
        }
        if(b==0){ //Si on veut aussi la regle et le filename
            changes = false;
            fileName = sfile;
        }

        updateTitle();
        qDebug() << (b==0 ? "Game opened" : b== 1 ? "Game imported" : "Rule imported");
        setEnabled(true);
    }
}

void MainWindow::newFile(){
    m_gm->reset();
    delete m_rule;
    m_rule = new GOLRule();
    m_game->setRule(m_rule);
    fileName = "";
    changes = false;
    updateTitle();
}

void MainWindow::backToLastFile()
{
    if(m_gm->running()){
        m_gm->startStop();
    }
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(this, tr("L'opération n'a pas pu être effectuée. Il se peut que le fichier indiqué soit protégé en lecture"),
            file.errorString());
        setEnabled(true);
        return;
    }
    QTextStream in(&file);
    Parser p(in);
    Sparse * S = p.sparse();
    m_game->load(*S);
    delete S;
    delete m_rule;
    m_rule = p.rule();
    m_game->setRule(m_rule);
    changes = false;

    updateTitle();
    qDebug() << ("Game backup done");
}

void MainWindow::setGen(unsigned int i)
{
    m_ui->nbGen->setText(QString::number(i));
}

void MainWindow::setPop(const unsigned int & i)
{
    m_ui->nbPop->setText(QString::number(i));
}

void MainWindow::setSpeedFromSpinBox(int new_val)
{

    if(new_val > m_ui->speedSlider->value() || new_val < m_ui->speedSlider->value())
    {
        m_ui->speedSlider->setValue(new_val);
        int new_interval = qRound(1000./double(new_val));
        m_gm->setInterval(new_interval);
    }

}

void MainWindow::openSettings()
{
    SettingsWindow settings(m_rule);
    setEnabled(false);
    if(m_gm->running()){
        m_gm->startStop();
    }
    settings.setModal(true);
    settings.exec();
    setEnabled(true);
}

void MainWindow::setSpeedFromSlider(int new_val)
{
    if(new_val > m_ui->speedSpinBox->value()|| new_val < m_ui->speedSpinBox->value())
    {
        int new_interval = qRound(1000./double(new_val));
        m_gm->setInterval(new_interval);
        m_ui->speedSpinBox->setValue(new_val);
    }
}


void MainWindow::updateIcon(bool running){
    m_ui->startButton->setIcon(running ? m_stopIcon : m_startIcon);
    m_ui->startButton->setToolTip(running ? "Mettre en pause [espace]": "Lancer le jeu [espace]");
}
