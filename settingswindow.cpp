#include "settingswindow.h"
#include "ui_settingswindow.h"

#include <QMessageBox>
#include <QRegularExpression>

SettingsWindow::SettingsWindow(Rule * rule, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsWindow),
    m_rule(rule)
{
    ui->setupUi(this);
    connect(ui->help,SIGNAL(clicked()), this,  SLOT(helpWindow()));
    this->setWindowIcon(QIcon(":/img/images/iconGameOfLife.gif"));
    this->setWindowTitle("Règles du jeu");
    presets = {
        ui->life,
        ui->highlife,
        ui->daynnight,
        ui->diamoeba,
        ui->persianrug,
        ui->longlife,
        ui->tache
    };
    ui->life->setToolTip("Règle de base deu jeu de la vie de Conway. De loin la plus connue et la plus étudiée.");
    ui->highlife->setToolTip("Similaire à la règle de base, mais avec l'existence d'un schéma appelé \"replicator\". Voir \"Game of life Replicator\"");
    ui->daynnight->setToolTip("Semble \"inverser\" le jeu : cellules mortes dans une mer de cellules vivantes.");
    ui->diamoeba->setToolTip("Créé des blobs en forme de diamant qui se comportent de manière imprévisible");
    ui->persianrug->setToolTip("Un simple bloc 2x2 produit un ensemble de motifs ressemblants à des tapis persans.");
    ui->longlife->setToolTip("Des oscilateurs de période très longue se produisent assez naturellement.");
    ui->tache->setToolTip("Des zones compactes de cellules vivantes se propagent de manière très lente.");



    connect(ui->annulerButton, SIGNAL(clicked()), this, SLOT(closeWindow()));
    connect(ui->validerButton, SIGNAL(clicked()), this, SLOT(accept()));

    for(size_t i = 0 ; i < presets.size() ; ++i){
        connect(presets[i], &QPushButton::clicked, this, [this, i]{setPreset(i);});
    }

    naissances = {
        ui->nait0,
        ui->nait1,
        ui->nait2,
        ui->nait3,
        ui->nait4,
        ui->nait5,
        ui->nait6,
        ui->nait7,
        ui->nait8,
    };
    survies = {
        ui->survive0,
        ui->survive1,
        ui->survive2,
        ui->survive3,
        ui->survive4,
        ui->survive5,
        ui->survive6,
        ui->survive7,
        ui->survive8,
    };
    for(size_t i = 0 ; i < 9 ; ++i){
        naissances[i]->setChecked(m_rule->birth()[i]);
        survies[i]->setChecked(m_rule->survive()[i]);
    }
    ui->statesSpinBox->setValue(m_rule->states()+1);
}

SettingsWindow::~SettingsWindow()
{
    delete ui;
}

void SettingsWindow::accept()
{
    qDebug()<<"accept";
    m_rule->survive() = {
        ui->survive0->isChecked(),
        ui->survive1->isChecked(),
        ui->survive2->isChecked(),
        ui->survive3->isChecked(),
        ui->survive4->isChecked(),
        ui->survive5->isChecked(),
        ui->survive6->isChecked(),
        ui->survive7->isChecked(),
        ui->survive8->isChecked()
    };
    m_rule->birth()={
        ui->nait0->isChecked(),
        ui->nait1->isChecked(),
        ui->nait2->isChecked(),
        ui->nait3->isChecked(),
        ui->nait4->isChecked(),
        ui->nait5->isChecked(),
        ui->nait6->isChecked(),
        ui->nait7->isChecked(),
        ui->nait8->isChecked()
    };
    m_rule->states() = ui->statesSpinBox->value()-1;
    closeWindow();
}

void SettingsWindow::closeWindow(){
    qDebug()<<"close";
    this->close();
}

void SettingsWindow::setPreset(size_t i){
    for(size_t k = 0; k < 9; ++k){
        survies[k]->setChecked(false);
        naissances[k]->setChecked(false);
    }
    switch (i) {
        case 0 : survies[2]->setChecked(true); survies[3]->setChecked(true);
                 naissances[3]->setChecked(true);
                 ui->statesSpinBox->setValue(2);
                 break;
        case 1 : survies[2]->setChecked(true); survies[3]->setChecked(true);
                 naissances[3]->setChecked(true); naissances[6]->setChecked(true);
                 ui->statesSpinBox->setValue(2);
                 break;
        case 2 : survies[3]->setChecked(true); survies[4]->setChecked(true); survies[6]->setChecked(true); survies[7]->setChecked(true); survies[8]->setChecked(true);
                 naissances[3]->setChecked(true); naissances[6]->setChecked(true);  naissances[7]->setChecked(true);  naissances[8]->setChecked(true);
                 ui->statesSpinBox->setValue(2);
                 break;
        case 3 : survies[5]->setChecked(true); survies[6]->setChecked(true); survies[7]->setChecked(true); survies[8]->setChecked(true);
                 naissances[3]->setChecked(true);naissances[5]->setChecked(true); naissances[6]->setChecked(true);  naissances[7]->setChecked(true);  naissances[8]->setChecked(true);
                 ui->statesSpinBox->setValue(2);
                 break;
        case 4 : naissances[2]->setChecked(true);naissances[3]->setChecked(true); naissances[4]->setChecked(true);
                 ui->statesSpinBox->setValue(2);
                 break;
        case 5 : survies[5]->setChecked(true);
                 naissances[3]->setChecked(true);naissances[5]->setChecked(true); naissances[4]->setChecked(true);
                 ui->statesSpinBox->setValue(2);
                 break;
        case 6 : survies[4]->setChecked(true); survies[6]->setChecked(true); survies[7]->setChecked(true); survies[8]->setChecked(true);
                 naissances[3]->setChecked(true); naissances[6]->setChecked(true);  naissances[7]->setChecked(true);  naissances[8]->setChecked(true);
                 ui->statesSpinBox->setValue(3);
                 break;
    }

}

void SettingsWindow::helpWindow()
{
    QMessageBox msgBox;
    msgBox.setText("Le nombre d'états représente le nombre de vies d'une cellule. Une cellule peut avoir (nombre d'état -1) vies. Chaque fois qu'une cellule survie, elle gagne une vie jusqu'a un maximum de (nombre d'état -1) vies. Quand elle doit mourrir, elle perd en réalité une vie et meurt vraiment si elle atteint 0 vies (etat 0).");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowIcon(QIcon(":/img/images/iconGameOfLife.gif"));
    msgBox.exec();
}


