#pragma once
#ifndef RULE_H
#define RULE_H
#include <vector>
#include <QString>
using namespace std;

/* Classe Rule :
    Une règle est un objet qui a une methode new_state(const vector<int> &)
    ou ce vecteur represente le voisinnage 3x3 d'un point.
    Une règle a aussi une methode ruleLine qui permet de recuperer la signature en qstring de cette regle dans un .lif
*/

class Rule{
public:
    virtual~Rule() = 0;
    virtual quint8 new_state(quint8 val, quint8 nb) = 0;
    virtual void debug()= 0;
    virtual QString ruleLine() = 0;
    virtual vector<bool> & birth() =0;
    virtual vector<bool> & survive() = 0;
    virtual quint8 & states() = 0;
};
#endif // RULE_H
