#pragma once
#ifndef PARSER_H
#define PARSER_H

#include <QObject>
#include <QFile>
#include "game.h"
#include "golrule.h"
#include <QRegularExpression>
#include <QRegularExpressionMatchIterator>
/* Classe Parser :

 Attributs :
    *une QString : le fichier lif
 Methodes clefs :
    *Sparse * sparse(), qui permet de recupérer le Sparse calculé
    * Rule * rule(), qui permet de recupérer la Rule calculée

 ATTENTION : le sparse et la rule sont renvoyés en allocation dynamique, il faut s'en occuper après !

*/
class Parser
{
private:
    QString m_lif;
public:
    GameScene m_gs;
    Parser(QTextStream & in):
        m_gs()
    {
        read_file(in);
    }

    ~Parser(){
    }

    void read_file(QTextStream & in){
       while (!in.atEnd())
       {
          QString line = in.readLine();
           m_lif = m_lif + "\n" + line;
       }
       m_lif += "\n";
        //qDebug()<<"content : "<<m_lif;
    }


    Sparse * sparse(){
        Sparse * m_sparse = new Sparse(&m_gs);

        QRegularExpression reg("#P\\s(\\+|-)?\\d+\\s(\\+|-)?\\d+\\n((\\.|\\*){1,80}\\n)*((\\.|\\*){1,80}(\\n?))");
        QRegularExpressionMatchIterator rxIterator = reg.globalMatch(m_lif);
        QStringList words;
        while (rxIterator.hasNext()) {
            QRegularExpressionMatch match = rxIterator.next();
            QString word = match.captured(0);
            words << word;
        }
        //
        //qDebug()<<m_lif;
        //qDebug()<<words;
        for(QString word : words){
            QRegularExpression reg("(\\+|-)?\\d+\\s(\\+|-)?\\d+");
            QRegularExpressionMatch match = reg.match(word);
            QString line = match.captured(0);
            QStringList parts = line.split(QRegularExpression("\\s+"));
            int base_i = parts[1].toInt();
            int base_j = parts[0].toInt();
            qDebug()<<parts;
            reg =  QRegularExpression("(\\.|\\*){1,80}\\n?");
            rxIterator = reg.globalMatch(word);
            int i = 0;
            QString test = "";
            while(rxIterator.hasNext()){
                QRegularExpressionMatch match = rxIterator.next();
                QString line = match.captured(0);
                test += line;
                qDebug()<<line;
                for(int j = 0 ; j < line.size() ; j++){
                    QChar c = line.at(j);
                    if(c == '*') m_sparse->set(base_j+j,  base_i+i, 1);
                    //else m_sparse.kill(base_i+i, base_j+j); Je sais pas trop si c'est ce qu'on veut
                }
                i++;
            }
            qDebug()<<test;
        }
        return m_sparse;
    }
    Rule * rule(){
        m_lif.replace("#N","#R 23/3/2");
        QRegularExpression reg("#R\\s\\d+/\\d+(/\\d+)?");
        QRegularExpressionMatch match = reg.match(m_lif);
        QString line = match.captured(0);
        QStringList parts = line.split(QRegularExpression("(#R\\s+)|/"));
        vector<bool> birth(9,0), survive(9,0);
        qDebug()<<line;
        qDebug()<<parts;
        for(int i = 0 ; i < parts[1].size() ; i++){
            unsigned int index = parts[1].at(i).digitValue();
            birth[index] = 1;
        }
        for(int i = 0 ; i < parts[2].size() ; i++){
            unsigned int index = parts[2].at(i).digitValue();
            survive[index] = 1;
        }
        quint8 states = 1;
        if(parts[3] != ""){
            states = quint8(parts[3].toUInt()-1);
        }
        Rule * r  = new GOLRule(birth, survive, states);
        //qDebug()<<"regle"<<r->ruleLine();
        return r;
    }


};

#endif // PARSER_H
