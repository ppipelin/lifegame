#pragma once
#ifndef SPARSE_H
#define SPARSE_H
#include <vector>
#include <QDebug>
#include <unordered_set>
#include <vector>
#include <QColor>
#include <QGraphicsItem>
#include "gamescene.h"
#include "cell.h"
/* Classe Sparse :
    Le Sparse contient toutes les informations relatives à l'etat du jeu (sans les règles quoi)

    Structures :
        Le Point : c'est un couple d'entier (i,j) + un voisinnage 3x3 de valeur autour (v) et un pointeur vers un QGraphicsItem,
            objet qui compose les QGraphicsScene. (ici une Cell)
        Le PointHasher : Objet qui permet d'indexer les points dans un unordered_set de manière efficace
            pas grand chose a dire dessus
    Attributs :
        m_mat : un unordered_set<Point,PointHasher> permet de stocker tous les points qui sont importants vis a vis de l'état du jeu
         -> On stocke tous les points de valeurs non nuls et tous les points dont au moins un voisin est non nul.
        m_cols : vecteur de couleurs pour savoir quelle couleur est associée a quelle valeur
        m_gs : un pointeur vers la scene dont ce sparse est associé. Ce sparse modifie cette scene aux appels de void set(i,j,val).
    Methodes (importantes) :
        void set(int i, int j, int val) : methode principale. Permet de modifier m_mat et m_gs en fonction des informations à conserver ou non
            ->modifie à la fois m_mat pour le modele "dur" et m_gs pour le modèle "de surface".
        clear : clear le sparse (m_mat et m_gs)
        merge : avec un autre sparse, prend les informations de sont m_mat pour les rajouter au sparse courant.
        load : clear + merge.

*/


using namespace std;

class Sparse :public QObject
{
public:
    struct Point{
        int i,j;
        //0 1 2
        //3 4 5
        //6 7 8
        //mutable vector<quint8> v;
        mutable quint8 val;
        mutable quint8 nb;
        mutable Cell * item;
        mutable vector<const Point *> v;
        Point(int i_,int j_, quint8 val_ =0, Cell * item_ = nullptr):i(i_),j(j_),val(val_),nb(0),item(item_)
        {
            v = vector< const Point*>(9,nullptr);
        }
        bool operator==(const Point & p) const{
            return i == p.i && j == p.j;
        }

    };
    struct PointHasher {
         PointHasher(){}
         std::size_t operator()(const Point & p) const{
             std::size_t h = 3*size_t(p.i) +5*size_t(p.j);
             return h;
         }
     };
     typedef unordered_set<Point,PointHasher> Mat;

private:
    Mat m_mat;
    GameScene * m_gs;
    unsigned int m_pop;
private:
    void link(Mat::iterator & it, const vector<bool>  & tolink){
        //Pour tous les voisins, les connecter entre eux... surement factorisable mais assez compliqué... donc autant tout faire en brut...
        //liaisons horitontales
        //0 <-> 1
        if(tolink[0]||tolink[1]){
            it->v[0]->v[5] = it->v[1];
            it->v[1]->v[3] = it->v[0];
        }

        //1 <-> 2
        if(tolink[1]||tolink[2]){
            it->v[1]->v[5] = it->v[2];
            it->v[2]->v[3] = it->v[1];
        }
        //6 <-> 7
        if(tolink[6]||tolink[7]){
            it->v[6]->v[5] = it->v[7];
            it->v[7]->v[3] = it->v[6];
        }
        //7 <-> 8
        if(tolink[7]||tolink[8]){
            it->v[7]->v[5] = it->v[8];
            it->v[8]->v[3] = it->v[7];
        }
        //liaisons verticales
        //2 <-> 5
        if(tolink[2]||tolink[5]){
            it->v[2]->v[7] = it->v[5];
            it->v[5]->v[1] = it->v[2];
        }
        //5 <-> 8
        if(tolink[5]||tolink[8]){
            it->v[5]->v[7] = it->v[8];
            it->v[8]->v[1] = it->v[5];
        }
        //0 <-> 3
        if(tolink[0]||tolink[3]){
            it->v[0]->v[7] = it->v[3];
            it->v[3]->v[1] = it->v[0];
        }
        //3 <-> 6
        if(tolink[3]||tolink[6]){
            it->v[3]->v[7] = it->v[6];
            it->v[6]->v[1] = it->v[3];
        }
        //liaisons diagonales montantes
        //3 <-> 1
        if(tolink[3]||tolink[1]){
            it->v[3]->v[2] = it->v[1];
            it->v[1]->v[6] = it->v[3];
        }
        //5 <-> 7
        if(tolink[5]||tolink[7]){
            it->v[7]->v[2] = it->v[5];
            it->v[5]->v[6] = it->v[7];
        }
        //liaisons diagonales descendantes
        //1 <-> 5
        if(tolink[5]||tolink[1]){
            it->v[1]->v[8] = it->v[5];
            it->v[5]->v[0] = it->v[1];
        }
        //3 <-> 7
        if(tolink[3]||tolink[7]){
            it->v[3]->v[8] = it->v[7];
            it->v[7]->v[0] = it->v[3];
        }
    }
    void linkall(Mat::iterator & it){
        link(it,vector<bool>(9,true));
    }


public:
    Sparse(GameScene * gs):
       m_gs(gs),m_pop(0)
    {
    }

    ~Sparse()
    {
        clear();
    }

    const unsigned int & pop() const{
        return m_pop;
    }

    void merge(const Sparse & S){
        for(Point p: S.m_mat){
            //On ne remplace pour quand il s'agit de mettre des cases blanches, c'est un choix
            if(p.val > 0) set(p.i,p.j,p.val);
        }
    }

    void load(const Sparse & S){
        clear();
        merge(S);
    }


    quint8 get(int i, int j){ // i :x , j : y dans l'affichage
        Mat::iterator it = m_mat.find(Point(i,j));
        return it == m_mat.end() ? 0 : it->val;
    }

    void set(int i, int j , quint8 val){
        //qDebug()<<i<<j<<"->"<<val;
        Mat::iterator it = m_mat.find(Point(i,j));
        // Si le point n'existe pas
        if(it==m_mat.end()){
            // Alors uniquement si la valeur que l'on ajoute est >0, on le rajoute
            if(val > 0){
                Cell * c = new Cell(i,j,val);
                m_gs->addItem(c);
                ++m_pop;
                /////
                Point pt(i,j,val,c); // Creation du point a inserer
                it = m_mat.insert(pt).first;
                quint8 cpt = 0;
                vector<bool> tolink(9,false);
                for(int l = -1 ; l <= 1 ; ++l){ // Pour chaques voisins
                    for(int c  = -1 ; c <= 1 ; ++c){
                        if(c != 0 || l !=0){
                            Point voisin = Point(i+c,j+l,0); //?
                            Mat::iterator itv = m_mat.find(voisin);
                            unsigned int index = 8 - cpt;
                            if(itv == m_mat.end()){ // Si il n'existe pas, on l'insere et on fait les connections des valeurs
                                itv = m_mat.insert(voisin).first;
                                itv->nb = 1;
                                tolink[cpt] = true;
                            }
                            else{ //Sinon, on fait juste les connections des valeurs
                                ++(itv->nb);
                                if(itv->val > 0) ++(it->nb);
                            }
                            it->v[cpt]= &(*itv);
                            itv->v[index]= &(*it);
                        }
                        ++cpt;
                    }
                }
                link(it,tolink);
            }
        }
        //Sinon, si le point existe
        else{
            quint8 mem = it->val;
            it->val = val;
            if(val > 0 && mem != val){ //Si on doit ajouter/changer un carré
                if(mem > 0){//Changer
                    it->item->update(val);
                    m_gs->update();
                }
                else//Ajouter
                {
                    Cell * c = new Cell(i,j,val);
                    m_gs->addItem(c);
                    it->item = c;
                    ++m_pop;
                    /////
                    quint8 cpt = 0;
                    vector<bool> tolink(9,false);
                    for(int l = -1 ; l <= 1 ; ++l){ // Pour chaques voisins
                        for(int c  = -1; c <= 1 ; ++c){
                            if(c != 0 || l !=0){
                                quint8 index = 8 - cpt;
                                const Point * vptr = it->v[cpt];
                                if(vptr==nullptr){
                                    Point voisin = Point(i+c,j+l,0); // ?
                                    Mat::iterator itv = m_mat.find(voisin);
                                    if(itv == m_mat.end()){
                                         itv = m_mat.insert(voisin).first;
                                         itv->nb = 1;
                                    }else{
                                        ++(itv->nb);
                                    }
                                    itv->v[index] = &(*it);
                                    it->v[cpt] = &(*itv);
                                    tolink[cpt] = true;
                                }else{
                                    ++(vptr->nb);
                                }
//ptite save on sait jamais...
//                                Point voisin = Point(i+l,j+c,0);
//                                Mat::iterator itv = m_mat.find(voisin);
//                                if(itv != m_mat.end()){ //Sinon, on fait juste les connections des valeurs
//                                    ++(itv->nb);
//                                }else{
//                                    voisin.nb = 1;
//                                    m_mat.insert(voisin);
//                                }
                            }
                            ++cpt;
                        }
                    }
                    link(it,tolink);
                }
            }else if(val == 0 && mem > 0){ //Si on doit supprimer un carré
                 m_gs->removeItem(it->item);
                 delete it->item;
                 --m_pop;
                 //// On enleve un nb aux 8 voisins de it
                 quint8 cpt = 0;
                 for(int l = -1 ; l <= 1 ; ++l){ // Pour chaques voisins
                     for(int c  = -1; c <= 1 ; ++c){
                         if(c != 0 || l !=0){
                            const Point * vptr = it->v[cpt];
                            --(vptr->nb);
                            if(vptr->nb == 0 && vptr->val ==0){
                            //pour tous les voisins de *vptr, leur signaler que vptr sera nullptr
                            for(size_t q = 0 ; q < 9 ; ++q){
                                if(vptr->v[q]!=nullptr && q !=4){
                                    vptr->v[q]->v[8-q] = nullptr;
                                }
                            }
                            m_mat.erase(m_mat.find(*vptr)); //pas ouf ca
                        }

//ptite save on sait jamais...
//                             Point voisin = Point(i+l,j+c,0);
//                             Mat::iterator itv = m_mat.find(voisin);
//                             if(itv != m_mat.end()){ //Sinon, on fait juste les connections des valeurs
//                                 --itv->nb;
//                                 if(itv->nb == 0 && itv->val ==0){
//                                    m_mat.erase(itv);
//                                 }
//                             }
                         }
                         ++cpt;
                     }
                 }
                 if(it->nb == 0){
                     //prevenir tous les voisins que l'on devient nullptr
                     for(size_t q = 0 ; q < 9 ; ++q){
                         if(it->v[q]!=nullptr && q !=4){
                             it->v[q]->v[8-q] = nullptr;
                         }
                     }
                     m_mat.erase(it);
                 }

            }
        }
    }

    void kill(int i, int j){
        set(i,j,0);
    }
    void clear(){
        m_gs->clear();
        m_mat.clear();
        m_pop = 0;
    }


    void debug(){
        for(auto it = m_mat.begin(); it!=m_mat.end(); ++it){
            int i = it->i;
            int j = it->j;
            int val = it->val;
            qDebug().nospace() << "["<<i<<"]["<<j<<"] -> "<<val <<" ("<<it->nb<<")";
        }
    }

    int size()const{
        return m_gs->items().size();
    }

    Mat::const_iterator cbegin(){
        return m_mat.cbegin();
    }
    Mat::const_iterator cend(){
        return m_mat.cend();
    }

};


#endif // SPARSE_H
