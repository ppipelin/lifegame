#include "helpwindow.h"
#include "ui_helpwindow.h"
#include <QIcon>

HelpWindow::HelpWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HelpWindow)
{
    clicDroit = new QPixmap(":/img/images/right-click.png");
    clicGauche = new QPixmap(":/img/images/left-click.png");
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":/img/images/iconGameOfLife.gif"));
    this->setWindowTitle("Aide");
//    ui->clicDroit->setScaledContents(true);
    int facteur = 3;
    ui->clicDroit->setPixmap(clicDroit->scaled(ui->clicDroit->width()*facteur, ui->clicDroit->height()*facteur,Qt::KeepAspectRatio));
    ui->clicGauche->setPixmap(clicGauche->scaled(ui->clicGauche->width()*facteur, ui->clicGauche->height()*facteur,Qt::KeepAspectRatio));
}

HelpWindow::~HelpWindow()
{
    delete clicDroit;
    delete clicGauche;
    delete ui;
}
