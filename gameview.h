#ifndef GAMEVIEW_H
#define GAMEVIEW_H
#pragma once
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QWidget>
#include <QDebug>
#include <QMouseEvent>
#include <QScrollBar>
#include <QApplication>
#include "gamescene.h"
#include "math.h"
/* Classe GameView :
  C'est elle qui gère la vue -> le deplacement et le zoom (en fait le zoom c'est la graphics_view_zoom mais c'est presque pareil)
*/

class GameView : public QGraphicsView
{
    Q_OBJECT
private:
    bool m_mooving;
    QPoint lastPos;
    QPoint newPos;
    QPoint m_center;
public:
    GameView(QWidget * parent = nullptr)
    :QGraphicsView (parent)
    {
        m_mooving = false;
        m_center = QPoint(width()/2, height()/2);
        setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    }
    ~GameView()
    {

    }
    void mousePressEvent(QMouseEvent *e){
        if(e->button() == Qt::MiddleButton){
            m_mooving = true;
            lastPos = e->pos();
            setCursor(Qt::ClosedHandCursor);
            //QApplication::setOverrideCursor(Qt::ClosedHandCursor);
        }else if(e->button() == Qt::LeftButton){
            QPointF p = mapToScene(e->pos());
            emit clicked(p, true);
        }else if(e->button()== Qt::RightButton){
            QPointF p = mapToScene(e->pos());
            emit clicked(p, false);
        }


    }
    void mouseReleaseEvent(QMouseEvent *e){
        if(e->button() == Qt::MiddleButton){
            m_mooving = false;
            setCursor(Qt::ClosedHandCursor);
            //QApplication::setOverrideCursor(Qt::ArrowCursor);
        }
        else if(e->button() == Qt::RightButton){
            emit released(false);
        }
        else if(e->button() == Qt::LeftButton){
            emit released(true);
        }
    }
    void mouseMoveEvent(QMouseEvent *e){

        newPos = e->pos();
        if(m_mooving){
            int hValue = horizontalScrollBar()->value() - (newPos.x() - lastPos.x());
            int vValue = verticalScrollBar()->value() - (newPos.y() - lastPos.y());
            horizontalScrollBar()->setValue(hValue);
            verticalScrollBar()->setValue(vValue);
            lastPos = newPos;
            e->accept();
            return;
        }
        emit mouseMoved(mapToScene(newPos));

        e->ignore();
    }



    void zoom(double factor){
        scale(factor,factor);
    }
public slots:
    void recenter(){
        qDebug()<<"recentrage";
        //fitInView(10,0,1000,700);

        centerOn(0,0);
        emit resetZoomSignal();
    }
signals:

void resetZoomSignal();
void clicked(const QPointF &, bool);
void released(bool);
void mouseMoved(const QPointF &);

};

#endif // GAMEVIEW_H
