#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QDialog>
#include "golrule.h"
#include <QDialogButtonBox>
#include <QPushButton>
#include <QCheckBox>

namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsWindow(Rule * rule, QWidget *parent = nullptr);
    ~SettingsWindow();
public slots:
    void accept();
    void closeWindow();
    void setPreset(size_t);
    void helpWindow();
private:
    Ui::SettingsWindow *ui;
    Rule * m_rule;
    vector<QCheckBox*> survies, naissances;
    vector<QPushButton*> presets;
};

#endif // SETTINGSWINDOW_H
