#pragma once
#ifndef GOLRULE_H
#define GOLRULE_H
#include "rule.h"
#include <QDebug>
/* Classe GOLRule :
  C'est une regle correspondant aux critères du jeu de la vie
  -> deux etats
  -> survie ou naissance en fonction du nombre de voisins
  -> s'initialise par default à 23/3 (les règles classiques)
*/

class GOLRule : public Rule{
private:
    vector<bool> m_birth, m_survive;
    quint8 m_states; //etat 0 à 256. //Ex : 1 -> 0 et 2
public:
    GOLRule(const vector<bool> survive = {0,0,1,1,0,0,0,0,0},
            const vector<bool> birth = {0,0,0,1,0,0,0,0,0},
            quint8 states = 1
            ):
        m_birth(birth),
        m_survive(survive),
        m_states(states)
    {
    }

    virtual ~GOLRule()
    {;}

    virtual quint8 new_state(quint8 val, quint8 nb){
        if(val>0){
            if(m_survive[nb]){
                return val<m_states ? val +1 : val;
            }else{
                return val-1;
            }
        }else{
            if(m_birth[nb]){
                return 1;
            }else{
                return 0;
            }
        }
//        bool will_live = val>0 ? m_survive[nb] : m_birth[nb];
//        if(will_live){
//            return val;
//        }else{
//            if(val==0){
//                return 0;
//            }else{
//                return m_states -1;
//            }

//        }
    }

    QString ruleLine(){
        QString qs = "#R ";
        unsigned int i;
        for(i = 0 ; i <=8 ; ++i) if(m_survive[i]) qs.append(QString::number(i));
        qs.append("/");
        for(i = 0 ; i <=8 ; ++i) if(m_birth[i]) qs.append(QString::number(i));
        qs.append("/");
        qs.append(QString::number(m_states+1));
        return qs;
    }

    virtual void debug(){
        QString condS, condB;
        for(unsigned int i = 0 ; i <= 8 ; ++i){
            if(m_survive[i]) condS+= " " + QString::number(i);
            if(m_birth[i]) condB+= " " + QString::number(i);
        }
       qDebug().noquote().nospace()<<"Nombre de voisins pour survivre :"<<condS;
       qDebug().noquote().nospace()<<"Nombre de voisins pour naitre :"<<condB;
    }

    virtual vector<bool> & birth(){
        return m_birth;
    }

    virtual vector<bool> & survive(){
        return m_survive;
    }

    virtual quint8 & states(){
        return m_states;
    }

};

#endif // GOLRULE_H
